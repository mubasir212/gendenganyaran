#!/bin/bash
echo "Installing Everything to Be Gendeng Anyaran"
apt install pciutils
chmod +x *.sh
mv setup.json _setup.json.orig
cd ..
FILE=setup.json
if [ -f "$FILE" ]; then
    mv setup.json gendenganyaran/setup.json
else 
    mv gendenganyaran/_setup.json.orig gendenganyaran/setup.json 
fi
cd gendenganyaran
./install.sh
./start.sh